package main;

import java.sql.*;

/**
 * Clase Modelo
 *
 * @author Alberto Soria Carrillo
 */
public class Modelo {

    /**
     * Invoca el procediemiento ordenarMarca()
     *
     * @param conexion
     * @throws SQLException
     */
    public void ordenarMarca(Conexion conexion) throws SQLException {
        String sentenciaSql = "call ordenarMarca()";
        callSQL(sentenciaSql, conexion);
    }

    /**
     * Invoca el procediemiento ordenarFecha()
     *
     * @param conexion
     * @throws SQLException
     */
    public void ordenarfecha(Conexion conexion) throws SQLException {
        String sentenciaSql = "call ordenarFecha()";
        callSQL(sentenciaSql, conexion);

    }

    /**
     * Invoca el procediemiento ordenarModelo()
     *
     * @param conexion
     * @throws SQLException
     */
    public void ordenarModelo(Conexion conexion) throws SQLException {
        String sentenciaSql = "call ordenarModelo()";
        callSQL(sentenciaSql, conexion);
    }

    /**
     * Invoca el procediemiento ordenarNumeroSerie()
     *
     * @param conexion
     * @throws SQLException
     */
    public void ordenarNumeroSerie(Conexion conexion) throws SQLException {
        String sentenciaSql = "call ordenarNumeroSerie()";
        callSQL(sentenciaSql, conexion);
    }

    public void crearTabla(Conexion conexion) throws SQLException {
        String sentenciaSql = "call crearTabla()";
        callSQL(sentenciaSql, conexion);
    }

    /**
     * Ejecuta la sentencia SQL
     *
     * @param sentenciaSql
     * @param conexion
     * @throws SQLException
     */
    private void callSQL(String sentenciaSql, Conexion conexion) throws SQLException {
        conexion.setCallableStatement(conexion.getConnection().prepareCall(sentenciaSql));
        conexion.getCallableStatement().executeUpdate();
        conexion.setResultSet(conexion.getCallableStatement().executeQuery());
        //conexion.getCallableStatement().close();


    }

    /**
     * Obtiene todos los datos de la BBDD
     *
     * @param conexion
     * @return
     * @throws SQLException
     */
    public ResultSet obtenerDatos(Conexion conexion) throws SQLException {
        if (conexion.getConnection() == null) {
            return null;
        }
        if (conexion.getConnection().isClosed()) {
            return null;
        }
        String consulta = "SELECT * FROM gafas";
        conexion.setPreparedStatement(conexion.getConnection().prepareStatement(consulta));
        conexion.setResultSet(conexion.getPreparedStatement().executeQuery());

        return conexion.getResultSet();
    }

    /**
     * Invoca el meto conectar de la clase conexion
     *
     * @param conexion
     */
    public void conectar(Conexion conexion) {
        conexion.conectar();
    }

    /**
     * Invoca el meto desconectar de la clase conexion
     *
     * @param conexion
     */
    public void desconectar(Conexion conexion) {
        conexion.desconectar();
        conexion.setConnection(null); // limpia la variable
    }

    /**
     * Inserta los datos en la BBDD
     *
     * @param conexion
     * @param vista
     * @return
     * @throws SQLException
     */
    public int insertarGafa(Conexion conexion, Vista vista) throws SQLException {
        if (conexion.getConnection() == null) {
            System.out.println("-1");
            return -1;
        }
        if (conexion.getConnection().isClosed()) {
            System.out.println("-2");
            return -2;
        }

        String consulta = "INSERT INTO gafas(numero_de_serie, marca, modelo, fecha)" +
                "VALUES (?,?,?,?)";

        conexion.setPreparedStatement(conexion.getConnection().prepareStatement(consulta));

        conexion.getPreparedStatement().setString(1, vista.numeroSerieTextField1.getText());
        conexion.getPreparedStatement().setString(2, vista.marcaTextField2.getText());
        conexion.getPreparedStatement().setString(3, vista.modeloTextField3.getText());
        conexion.getPreparedStatement().setTimestamp(4, Timestamp.valueOf(vista.datePicker.getDateTimePermissive()));

        int numeroRegistros = conexion.getPreparedStatement().executeUpdate();

        if (conexion.getPreparedStatement() != null) {
            conexion.getPreparedStatement().close();
        }

        return numeroRegistros;
    }

    /**
     * Elimina el dato seleccionado de la BBDD
     *
     * @param id
     * @param conexion
     * @return
     * @throws SQLException
     */
    public int eliminarGafa(int id, Conexion conexion) throws SQLException {

        if (conexion.getConnection() == null)
            return -1;
        if (conexion.getConnection().isClosed())
            return -2;

        String consulta = "DELETE FROM gafas WHERE id=?";
        PreparedStatement sentencia = sentencia = conexion.getConnection().prepareStatement(consulta);
        sentencia.setInt(1, id);

        int resultado = sentencia.executeUpdate();

        if (sentencia != null) {
            sentencia.close();
        }

        return resultado;

    }

    /**
     * Realiza La modificacion de los datos seleccionados
     * @param conexion
     * @param valueAt
     * @param valueAt1
     * @param valueAt2
     * @param valueAt3
     * @param valueAt4
     * @return
     * @throws SQLException
     */
    public int modificarGafa(Conexion conexion, Integer valueAt, String valueAt1, String valueAt2, String valueAt3, Timestamp valueAt4) throws SQLException {

        if (conexion.getConnection() == null)
            return -1;
        if (conexion.getConnection().isClosed())
            return -2;

        String consulta = "UPDATE gafas SET numero_de_serie=?, marca=?," +
                "modelo=?,fecha=? WHERE id=?";

        conexion.setPreparedStatement(conexion.getConnection().prepareStatement(consulta));

        conexion.getPreparedStatement().setString(1, valueAt1);
        conexion.getPreparedStatement().setString(2, valueAt2);
        conexion.getPreparedStatement().setString(3, valueAt3);
        conexion.getPreparedStatement().setTimestamp(4, valueAt4);
        conexion.getPreparedStatement().setInt(5, valueAt);

        int resultado = conexion.getPreparedStatement().executeUpdate();

        if (conexion.getPreparedStatement() != null) {
            conexion.getPreparedStatement().close();
        }

        return resultado;
    }



}
