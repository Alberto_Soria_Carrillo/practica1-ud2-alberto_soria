package main;

import java.sql.*;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 * Clase Conexion. Encargada de establecer la conexion con la base de datos
 *
 * @author Alberto Soria Carrillo
 */
public class Conexion {

    // Atributos
    private Datos dato; // Guarda la informcaion del programa
    private Connection connection;
    private PreparedStatement preparedStatement;
    private ResultSet resultSet;
    private CallableStatement callableStatement; // Para invocar procedimientos

    // Getters and Setters
    public Connection getConnection() {
        return connection;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    public void setResultSet(ResultSet resultSet) {
        this.resultSet = resultSet;
    }

    public PreparedStatement getPreparedStatement() {
        return preparedStatement;
    }

    public void setPreparedStatement(PreparedStatement statement) {
        this.preparedStatement = statement;
    }

    public ResultSet getResultSet() {
        return resultSet;
    }

    public CallableStatement getCallableStatement() {
        return callableStatement;
    }

    public void setCallableStatement(CallableStatement callableStatement) {
        this.callableStatement = callableStatement;
    }

    // Constructor
    public Conexion(Datos dato) {
        this.dato = dato;
    }

    /**
     * Encargado de la conexion
     */
    public void conectar() {
        try {
            String url = "jdbc:mysql://" + dato.getIpServer() + ":" + dato.getPuertoServer() + "/" + dato.getNombreBBDD()
                    + "?serverTimezone=UTC";

            Class.forName("com.mysql.cj.jdbc.Driver"); // controlador nuevo
            // Class.forName("com.mysql.jdbc.Driver"); //controlador antiguo deprecado

            this.connection = DriverManager.getConnection(url, dato.getUsurarioBBDD(), dato.getPaswordUsuario());

        } catch (SQLException | ClassNotFoundException e) {
            JOptionPane.showMessageDialog(null, "Fallo en la conexión");
            e.printStackTrace();
        }
    }

    /**
     * Encargado de la desconexion
     */
    public void desconectar() {
        try {
            this.connection.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Fallo en la desconexión");
            e.printStackTrace();
        }
    }

    /**
     * Realiza la busqueda del numero de serie introducido
     *
     * @param conexion
     * @param text
     */
    public void buscar(Conexion conexion, String text) throws SQLException {

        String query = "select * from gafas where numero_de_serie=\'" + text.trim() +"\'";
        preparedStatement = connection.prepareStatement(query);
        resultSet = preparedStatement.executeQuery();

    }
}

