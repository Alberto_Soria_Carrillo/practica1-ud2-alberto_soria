package main;

import javax.swing.*;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.sql.Timestamp;

/**
 * Clase Controlador
 *
 * @author Alberto Soria Carrillo
 */
public class Controlador implements ActionListener, TableModelListener {
    //Atributos
    private Datos dato;
    private Modelo modelo;
    private Vista vista;
    private Conexion conexion;

    // Constructor
    public Controlador(Datos dato, Modelo modelo, Vista vista) {

        this.dato = dato;
        this.modelo = modelo;
        this.vista = vista;
        this.conexion = new Conexion(dato);

        iniciarTablas();
        addActionListener(this);
        addTableModelListeners(this);
    }

    /**
     * Agrega el TableModelListener a las tablas
     *
     * @param listener
     */
    private void addTableModelListeners(TableModelListener listener) {

        vista.dtm1.addTableModelListener(listener);
        vista.dtm2.addTableModelListener(listener);

    }

    /**
     * Agrega el ActionListener a los bototnes
     *
     * @param listener
     */
    private void addActionListener(ActionListener listener) {

        vista.buscarButton.addActionListener(listener);
        vista.eliminarButton.addActionListener(listener);
        vista.nuevoButton.addActionListener(listener);
        vista.ordenarButton.addActionListener(listener);
        vista.numeroDeSerieRadioButton.addActionListener(listener);
        vista.modeloRadioButton.addActionListener(listener);
        vista.fechaRadioButton.addActionListener(listener);
        vista.marcaRadioButton.addActionListener(listener);
        vista.itemConexion.addActionListener(listener);
        vista.itemSalir.addActionListener(listener);
        vista.itemConfig.addActionListener(listener);
        vista.itemTabla.addActionListener(listener);

    }

    /**
     * Agrega los titulos de las columnas a las tablas
     */
    private void iniciarTablas() {

        String[] headers = {"Id", "Numero de serie", "Marca", "Modelo", "Fecha"};
        vista.dtm1.setColumnIdentifiers(headers);
        vista.dtm2.setColumnIdentifiers(headers);
    }


    @Override
    public void actionPerformed(ActionEvent e) {

        if (e.getSource() == vista.itemConexion) {

            if (conexion.getConnection() == null) {
                modelo.conectar(conexion);
                try {
                    cargarFilas(conexion, vista.dtm1);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            } else {
                modelo.desconectar(conexion);
            }

            if (conexion.getConnection() != null) {
                vista.itemConexion.setText("Desconectar");
            } else {
                vista.itemConexion.setText("Conectar");
                vista.dtm1.setRowCount(0);
            }
        }

        if (e.getSource() == vista.itemConfig) {
            VistaConfig vistaConfig = new VistaConfig(dato);
        }

        if (e.getSource() == vista.itemTabla){
            try {
                modelo.crearTabla(conexion);
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }

        if (e.getSource() == vista.itemSalir) {
            System.exit(0);
        }

        if (e.getSource() == vista.buscarButton) {
            if (!vista.buscarTextField4.getText().equalsIgnoreCase("")
                    && !vista.buscarTextField4.getText().isEmpty()) {
                try {
                    conexion.buscar(conexion, vista.buscarTextField4.getText());
                    cargarFilasOrdenadas(conexion, vista.dtm2);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            } else {
                JOptionPane.showMessageDialog(null, "Debes introducir algun dato en el campo busqueda");
            }
        }

        if (e.getSource() == vista.eliminarButton) {
            try {
                int filaBorrar = vista.table1.getSelectedRow();
                int idBorrar = (Integer) vista.dtm1.getValueAt(filaBorrar, 0);
                modelo.eliminarGafa(idBorrar, conexion);
                vista.dtm1.removeRow(filaBorrar);

            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }

        if (e.getSource() == vista.nuevoButton) {
            if (vista.numeroSerieTextField1.getText().equals("") || vista.marcaTextField2.getText().equals("") ||
                    vista.modeloTextField3.getText().equals("") || vista.datePicker.getDateTimePermissive().equals("")) {
                JOptionPane.showMessageDialog(null, "Debes rellenar todos los campos");
            } else {
                try {

                    modelo.insertarGafa(conexion, vista);
                    cargarFilas(conexion, vista.dtm1);
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
        }


        if (e.getSource() == vista.ordenarButton && conexion.getConnection() != null) {
            if (!vista.marcaRadioButton.isSelected() &&
                    !vista.fechaRadioButton.isSelected() &&
                    !vista.modeloRadioButton.isSelected() &&
                    !vista.numeroDeSerieRadioButton.isSelected()) {
                JOptionPane.showMessageDialog(null, "Debes rellenar todos los campos");
            } else {
                try {
                    if (vista.marcaRadioButton.isSelected()) {
                        modelo.ordenarMarca(conexion);
                        cargarFilasOrdenadas(conexion, vista.dtm2);
                    }

                    if (vista.fechaRadioButton.isSelected()) {
                        modelo.ordenarfecha(conexion);
                        cargarFilasOrdenadas(conexion, vista.dtm2);
                    }

                    if (vista.modeloRadioButton.isSelected()) {
                        modelo.ordenarModelo(conexion);
                        cargarFilasOrdenadas(conexion, vista.dtm2);
                    }

                    if (vista.numeroDeSerieRadioButton.isSelected()) {
                        modelo.ordenarNumeroSerie(conexion);
                        cargarFilasOrdenadas(conexion, vista.dtm2);
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    /**
     * Inserta la información ordenada en la variable dtm2
     *
     * @param conexion
     * @param dtm
     * @throws SQLException
     */
    private void cargarFilasOrdenadas(Conexion conexion, DefaultTableModel dtm) throws SQLException {

        Object[] fila = new Object[5];
        dtm.setRowCount(0);
        while (conexion.getResultSet().next()) {

            fila[0] = conexion.getResultSet().getInt(1);
            fila[1] = conexion.getResultSet().getString(2);
            fila[2] = conexion.getResultSet().getString(3);
            fila[3] = conexion.getResultSet().getString(4);
            fila[4] = conexion.getResultSet().getTimestamp(5);

            dtm.addRow(fila);

        }
        conexion.getResultSet().close();
    }

    /**
     * Inserta la información en la variable dtm1
     *
     * @param conexion
     * @param dtm
     * @throws SQLException
     */
    private void cargarFilas(Conexion conexion, DefaultTableModel dtm) throws SQLException {

        Object[] fila = new Object[5];
        dtm.setRowCount(0);
        String consulta = "select * from gafas";
        conexion.setPreparedStatement(conexion.getConnection().prepareStatement(consulta));
        conexion.setResultSet(conexion.getPreparedStatement().executeQuery());
        while (conexion.getResultSet().next()) {
            fila[0] = conexion.getResultSet().getInt(1);
            fila[1] = conexion.getResultSet().getString(2);
            fila[2] = conexion.getResultSet().getString(3);
            fila[3] = conexion.getResultSet().getString(4);
            fila[4] = conexion.getResultSet().getTimestamp(5);

            dtm.addRow(fila);
        }
        conexion.getResultSet().close();
    }

    @Override
    public void tableChanged(TableModelEvent e) {

        if (e.getType() == TableModelEvent.UPDATE) {
            int filaModicada = e.getFirstRow();
            try {
                modelo.modificarGafa(conexion, (Integer)vista.dtm1.getValueAt(filaModicada,0),
                                                (String)vista.dtm1.getValueAt(filaModicada,1),
                                                (String)vista.dtm1.getValueAt(filaModicada,2),
                                                (String)vista.dtm1.getValueAt(filaModicada,3),
                                                Timestamp.valueOf((String) vista.dtm1.getValueAt(filaModicada,4)));
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }

    }
}
