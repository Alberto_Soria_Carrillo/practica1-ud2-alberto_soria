package main;

import com.github.lgooddatepicker.components.DateTimePicker;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

/**
 * Clase Vista. Ventana principal del programa donde se realiza la mayoria de las acciones o tareas
 *
 * @author Alberto Soria Carrillo
 */
public class Vista extends JFrame{
    // Atributos
    JPanel panel1;
    JTextField numeroSerieTextField1;
    JTextField marcaTextField2;
    JTextField modeloTextField3;
    JButton buscarButton;
    JButton nuevoButton;
    JButton eliminarButton;
    JTable table1;
    JTable table2;
    JTextField buscarTextField4;
    JButton ordenarButton;
    JRadioButton numeroDeSerieRadioButton;
    JRadioButton marcaRadioButton;
    JRadioButton modeloRadioButton;
    JRadioButton fechaRadioButton;
    DateTimePicker datePicker;
    DefaultTableModel dtm1;
    DefaultTableModel dtm2;
    JMenuBar menuBar;
    JMenu menu;
    JMenuItem itemConexion;
    JMenuItem itemConfig;
    JMenuItem itemTabla;
    JMenuItem itemSalir;

    // Constructor
    public Vista() {

        this.setTitle("Optica");
        this.setContentPane(panel1);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        dtm1 =new DefaultTableModel();
        dtm2 =new DefaultTableModel();
        table1.setModel(dtm1);
        table2.setModel(dtm2);

        crearMenu();

        this.pack();
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
    }

    /**
     * Crea un menu y le agrega los elementos necesarios para el programa
     */
    private void crearMenu() {
        menuBar = new JMenuBar();
        menu = new JMenu("Archivo");
        itemConexion = new JMenuItem("Conectar");
        itemConfig = new JMenuItem("Configuracion");
        itemTabla = new JMenuItem("Crear tabla gafas");
        itemSalir = new JMenuItem("Salir");

        menu.add(itemConexion); // conecta/desconecta el programa de la BBDD
        menu.add(itemConfig); // abre la ventana de configuración
        menu.add(itemTabla); // llama al procedimiento para crear la tabla gafas
        menu.add(itemSalir); // sale del programa
        menuBar.add(menu);

        this.setJMenuBar(menuBar);

    }
}
