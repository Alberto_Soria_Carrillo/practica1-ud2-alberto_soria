CREATE database Optica;
USE Optica;

CREATE TABLE gafas(
id int primary key auto_increment,
numero_de_serie varchar(25),
marca varchar(25),
modelo varchar(25),
fecha timestamp);

insert into gafas(numero_de_serie,marca,modelo,fecha) value('prueba1','prueba1','prueba1','1985-07-13');

select * from gafas;