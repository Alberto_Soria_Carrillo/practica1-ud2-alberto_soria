call ordenarMarca();
call ordenarFecha();
call ordenarModelo();
call ordenarNumeroSerie();
DELIMITER //
create procedure ordenarMarca()
BEGIN
SELECT * FROM gafas order by gafas.marca;
END //
DELIMITER //
create procedure ordenarFecha()
BEGIN
SELECT * FROM gafas order by gafas.fecha;
END //
DELIMITER //
create procedure ordenarModelo()
BEGIN
SELECT * FROM gafas order by gafas.modelo;
END //
DELIMITER //
create procedure ordenarNumeroSerie()
BEGIN
SELECT * FROM gafas order by gafas.numero_de_serie;
END //
DELIMITER //
CREATE PROCEDURE crearTabla()
BEGIN
	CREATE TABLE gafas(
		id int primary key auto_increment,
		numero_de_serie varchar(25),
		marca varchar(25),
		modelo varchar(25),
		fecha timestamp);
END //